package pucrs.progoo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AppTeste {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		Obra obra1 = new Obra(0, "Seilá", "opa", 1);
		
		Acervo acervo = new Acervo();
		Cadastro cadastro = new Cadastro();
		Emprestimos listaEmp = new Emprestimos();
		
		System.out.println("Usuarios:"+Usuario.getTotalUsuarios());

		cadastro.cadastrar("1", "Fulano", "01929123132");
		cadastro.cadastrar("2", "Beltrano", "82816612122");
		cadastro.cadastrar("3", "Ciclano", "91277626212");
		cadastro.cadastrar("4", "Huguinho", "28156483929");
		cadastro.cadastrar("5", "Zezinho", "71626846671");
		cadastro.cadastrar("6", "Luizinho", "77691761727");
		cadastro.ordenaNome();
		
		System.out.println("Usuarios:"+Usuario.getTotalUsuarios());

		acervo.cadastrarObra(new Livro(1, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 2000, 5));
		acervo.cadastrarObra(new Livro(2, "A Origem das Espécies", "Charles Darwin", "Hemus", 2000, 2));
		acervo.cadastrarObra(new Livro(3, "A Arte da Guerra", "Sun Tzu", "Pensamento", 1995, 3));
		acervo.cadastrarObra(new Periodico(6, "Java 2016", "Ed. Minha", 2016, 1, 4));
		// Exemplo do construtor sobrecarregado (assume total = 1)
		acervo.cadastrarObra(new Livro(4, "Memórias Póstumas de Brás Cubas", "Machado de Assis", "Ática", 1998));
		acervo.cadastrarObra(new Livro(5, "O Senhor dos Anéis", "J. R. R. Tolkien", "Martins Fontes", 1958, 1));
		acervo.ordenaTitulo();
//		acervo.ordenaCodigo();

		System.out.println("Usuários:");
		cadastro.listarTodos();

		System.out.println();
		System.out.println("Acervo:");
		acervo.listarTodos();
		
		Scanner in = new Scanner(System.in);
		Usuario user = null;
		int opcao = 0;
		do {

			System.out.println("*** BIBLIOTECA ***");
			System.out.println();

			if (user != null)
				System.out.println("Usuário ativo: " + user.getNome());

			System.out.println("1 - Escolher usuário");
			System.out.println("2 - Listar empréstimos");
			System.out.println("3 - Retirar um livro");
			System.out.println("4 - Devolver um livro");
			System.out.println("0 - Sair");
			System.out.print("Opção: ");

			opcao = in.nextInt();

			if (opcao == 1) {
				System.out.println("Usuários:");
				System.out.println(cadastro.listarTodos());
				System.out.print("Selecione: ");
				int codigo = in.nextInt();
				if (codigo > 0 && codigo <= cadastro.getTotal())
					user = cadastro.buscarCodigo("" + codigo);
			}

			else if (opcao == 2) {
				if (user == null) {
					System.out.println("Primeiro selecione um usuário!");
					System.out.println();
					continue;
				}
				ArrayList<Emprestimo> lista = listaEmp.buscarPorUsuario(user);
				System.out.println();
				System.out.println("Empréstimos:");
				for (Emprestimo emp : lista) {
					System.out.println(emp);
				}
			}

			else if (opcao == 3) {
				if (user == null) {
					System.out.println("Primeiro selecione um usuário!");
					System.out.println();
					continue;
				}
				System.out.println();
				System.out.println(acervo.listarTodos());
				System.out.print("Obra desejada: ");
				int codobra = in.nextInt();
				Obra obra = acervo.buscarPorCodigo(codobra);
				if (obra == null) {
					System.out.println("Código inválido!");
					continue;
				}
				if (obra.emprestar()) {
					LocalDate dataEntrega = LocalDate.now().plusDays(7);
					listaEmp.criar(user, obra, dataEntrega);
					System.out.println("*** Livro retirado!");
				} else
					System.out.println(">>> ERRO: não foi possível retirar o livro!");
			}

			else if (opcao == 4) {
				if (user == null) {
					System.out.println("Primeiro selecione um usuário!");
					System.out.println();
					continue;
				}
				ArrayList<Emprestimo> lista = listaEmp.buscarPorUsuario(user);
				System.out.println();
				System.out.println("Empréstimos:");
				int cont = 1;
				for (Emprestimo emp : lista) {
					System.out.println(cont + ": " + emp);
					cont++;
				}
				System.out.print("Qual você deseja devolver? ");
				int numEmp = in.nextInt();
				if (numEmp >= 1 && numEmp <= lista.size()) {
					Emprestimo emp = lista.get(numEmp - 1);
					boolean ok = emp.finalizar(LocalDate.now());
					if(ok)
						System.out.println("Empréstimo finalizado!");
					else
						System.out.println(">>> ERRO: Empréstimo já devolvido!");
				} else
					System.out.println(">>> Empréstimo inexistente!");
			}

			System.out.println();

			// cadastro.ordenaNome();
			// System.out.println();
			// System.out.println("Acervo:");
			// System.out.println(acervo.listarTodos());
			// acervo.ordenaTitulo();
			// System.out.println("Acervo ordenado por título:");
			// System.out.println(acervo.listarTodos());
			// acervo.ordenaCodigo();
			// System.out.println("Acervo ordenado por código:");
			// System.out.println(acervo.listarTodos());
		} while (opcao != 0);
		
	/*	
		// Exemplo: criando um empréstimo
		LocalDate dataLim = LocalDate.of(2016, 3, 23);
		LocalDate dataLim2= LocalDate.now().plusDays(7);
		//System.out.println(dataLim2);
		// Não esqueça de obter uma ref. para um Usuario e Livro
		Usuario usuario = cadastro.buscarCodigo("4");
		Livro livro = acervo.buscarPorCodigo(3);
		listaEmp.criar(usuario, livro, dataLim);
		
		System.out.println();
		listaEmp.listarTodos();
		
		System.out.println();
		Periodico per1 = new Periodico(10, "PUCRS Informação", "EDIPUCRS", 2015, 18, 4);
		System.out.println(per1);
		per1.emprestar();
		System.out.println(per1);
		
		ArrayList<Integer> lista = new ArrayList<>();
		lista.add(12);
		lista.add(7);
		lista.add(4);
		lista.add(8);
		lista.add(2);
		lista.add(9);
		Collections.sort(lista);
		System.out.println(lista);
	*/			
	}

}
