package pucrs.progoo;

public class Periodico extends Obra {
	
	private int volume;
	private int numero;
	private boolean emprestado;

	public Periodico(int codigo, String titulo, String editora, int ano, int volume, int numero) {
		super(codigo, titulo,editora, ano);		
		this.volume = volume;
		this.numero = numero;
		this.emprestado = false;
	}

	public int getVolume() {
		return volume;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public boolean isEmprestado() {
		return emprestado;
	}	

	@Override
	public String toString() {
		return super.toString()+" "+volume+" ("+numero+")";
	}

	@Override
	public boolean emprestar() {
		if(!emprestado) {
            emprestado = true;
            return true;
        }
        return false;
	}

	@Override
	public boolean devolver() {
		if(emprestado) {
            emprestado = false;
            return true;
        }
        return false;
	}
}
