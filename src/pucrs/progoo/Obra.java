package pucrs.progoo;

public abstract class Obra implements Emprestavel, Comparable<Obra> {

	private int codigo;
	private String titulo;
	private String editora;
	private int ano;
	
	public Obra(int codigo, String titulo, String editora, int ano) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.editora = editora;
		this.ano = ano;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getEditora() {
		return editora;
	}

	public int getAno() {
		return ano;
	}
	
	@Override
    public String toString() {
		return String.format("%03d: %-40s %-20s %-5d",
				codigo, titulo, editora, ano);        
    }
	
	@Override
	public int compareTo(Obra o) {
		return titulo.compareTo(o.titulo);
	}
}
