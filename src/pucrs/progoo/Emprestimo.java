package pucrs.progoo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Emprestimo implements Comparable<Emprestimo> {

	private Usuario usuario;
	private Obra obra;
	private LocalDate dataLimite;
	private LocalDate dataDevolucao;

	public Emprestimo(Usuario usuario, Obra obra, LocalDate dataLimite) {
		this.usuario = usuario;
		this.obra = obra;
		this.dataLimite = dataLimite;
		this.dataDevolucao = null;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public Obra getObra() {
		return obra;
	}

	public LocalDate getDataLimite() {
		return dataLimite;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	@Override
	public String toString() {
		String dataLimiteStr = dataLimite.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));		
		String dataDevolucaoStr = "";
		if(dataDevolucao != null)
			dataDevolucaoStr = "até" + dataDevolucao.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
		String aux = String.format("%-30s -> %-20s - %s %s", obra.getTitulo(), usuario.getNome(), dataLimiteStr,
				dataDevolucaoStr);
		return aux;
	}

	@Override
	public int compareTo(Emprestimo o) {
		return dataLimite.compareTo(dataLimite);
	}

	public boolean finalizar(LocalDate dataDevolucao) {
		if(this.dataDevolucao != null)
			return false;
		this.dataDevolucao = dataDevolucao;
		return true;
	}
}
